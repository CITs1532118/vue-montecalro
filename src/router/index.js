import Vue from 'vue'
import Router from 'vue-router'
import Montecalro from '@/components/Montecalro'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Montecalro',
      component: Montecalro
    }
  ]
})
